﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelMaker : MonoBehaviour
{
    Coroutine maker = null;

    public RoomScript startRoomPrefab;
    public RoomScript endRoomPrefab;
    public List<RoomScript> roomPrefabs = new List<RoomScript>();

    public Vector2 iterationRange = new Vector2(3, 10);

    List<DoorScript> availDoors = new List<DoorScript>();

    StartRoom starter;
    EndRoom ender;
    List<RoomScript> placedRooms = new List<RoomScript>();

    LayerMask roomMask;

    private void Start()
    {
        roomMask = LayerMask.GetMask("Room");
        maker = StartCoroutine("GenerateLevel");
    }

    IEnumerator GenerateLevel()
    {
        WaitForSeconds startup = new WaitForSeconds(1);
        WaitForFixedUpdate interval = new WaitForFixedUpdate();

        yield return startup;

        //place starter
        PlaceStartRoom();
        yield return interval;

        // random iterations
        int iterations = Random.Range((int)iterationRange.x, (int)iterationRange.y);

        for(int i = 0; i < iterations; i++)
        {
            //place random rooms from list
            MakeRoom();
            yield return interval;
        }

        // place end room
        PlaceEndRoom();
        yield return interval;

        Debug.Log("Level Generation Completed");
        // then place player & such.....................

        yield return null;
    }

    void PlaceStartRoom()
    {
        Debug.Log("Place Start Room");
        starter = Instantiate(startRoomPrefab) as StartRoom;
        starter.transform.parent = this.transform;

        // get doors
        DoorCompiler(starter, ref availDoors);

        // position room.
        starter.transform.position = Vector3.zero;
        starter.transform.rotation = Quaternion.identity;
    }

    void MakeRoom()
    {
        Debug.Log("Placing random room!");
        //instantiate
        RoomScript currentRoom = 
            Instantiate(roomPrefabs[Random.Range(0, roomPrefabs.Count)]) as RoomScript;
        currentRoom.transform.parent = this.transform;

        // create doorway list to loop over
        List<DoorScript> allAvailableDoors = new List<DoorScript>(availDoors);
        List<DoorScript> currentDoors = new List<DoorScript>();
        DoorCompiler(currentRoom, ref currentDoors);

        // get doors from current room & add randomly
        //           to the list of avail. doors
        DoorCompiler(currentRoom, ref availDoors);

        bool roomPlaced = false;

        foreach(DoorScript d in allAvailableDoors)
        {
            // try all avail. doors in this room
            foreach(DoorScript c in currentDoors)
            {
                // position
                PositionRoom(ref currentRoom, c, d);

                // check for overlap
                if (CheckRoomOverlap(currentRoom))
                {
                    continue;
                }
                roomPlaced = true;

                // add room to list
                placedRooms.Add(currentRoom);

                // remove occupied doors
                c.gameObject.SetActive(false);
                availDoors.Remove(c);
                d.gameObject.SetActive(false);
                availDoors.Remove(d);

                // exit loop
                break;
            }
            if (roomPlaced)
            {
                break;
            }
        }

        // room couldn't be placed,
        // restart generator
        // try again.
        if (!roomPlaced)
        { 
            Destroy(currentRoom.gameObject);
            ResetLevelGen();
        }
    }

    void PlaceEndRoom()
    {
        Debug.Log("Place End Room");
        ender = Instantiate(endRoomPrefab) as EndRoom;
        ender.transform.parent = this.transform;

        // create doorway list to loop over
        List<DoorScript> allAvailableDoors = new List<DoorScript>(availDoors);
        DoorScript door = ender.doorways[0];
        bool roomPlaced = false;

        foreach(DoorScript a in allAvailableDoors)
        {
            RoomScript r = (RoomScript)ender;
            PositionRoom(ref r, door, a);

            if (CheckRoomOverlap(ender))
            {
                continue;
            }
            roomPlaced = true;

            door.gameObject.SetActive(false);
            availDoors.Remove(door);
            a.gameObject.SetActive(false);
            availDoors.Remove(a);

            break;
        }
        if (!roomPlaced)
        {
            Destroy(ender.gameObject);
            ResetLevelGen();
        }
    }

    void ResetLevelGen()
    {
        Debug.Log("Restarting level generation!!!");

        StopCoroutine(maker);

        // clean up
        if (starter)
        {
            Destroy(starter.gameObject);
        }
        if (ender)
        {
            Destroy(ender.gameObject);
        }
        foreach(RoomScript r in placedRooms)
        {
            Destroy(r.gameObject);
        }

        // clear all lists
        placedRooms.Clear();
        availDoors.Clear();

        // reset
        maker = StartCoroutine("GenerateLevel");
    }

    void DoorCompiler(RoomScript room, ref List<DoorScript> l)
    {
        foreach(DoorScript d in room.doorways)
        {
            int r = Random.Range(0, l.Count);
            l.Insert(r, d);
        }
    }

    void PositionRoom(ref RoomScript r, DoorScript d, DoorScript target)
    {
        // reset pos
        r.transform.position = Vector3.zero;
        r.transform.rotation = Quaternion.identity;

        //rotate room to match prev. door orientation
        Vector3 targetDoorEuler = target.transform.eulerAngles;
        Vector3 nextDoorEuler = d.transform.eulerAngles;
        float deltaAngle = Mathf.DeltaAngle(nextDoorEuler.y, targetDoorEuler.y);
        Quaternion currentTargetRotation = Quaternion.AngleAxis(deltaAngle, Vector3.up);
        r.transform.rotation = currentTargetRotation * Quaternion.Euler(0, 180f, 0);

        // position room
        Vector3 roomPosOffset = d.transform.position - r.transform.position;
        r.transform.position = target.transform.position - roomPosOffset;
    }

    bool CheckRoomOverlap(RoomScript r)
    {
        Bounds bounds = r.RoomBounds;
        bounds.Expand(-0.1f); // ?
        bounds.center = r.transform.position;

        Collider[] colliders =
            Physics.OverlapBox(bounds.center, bounds.size / 2, r.transform.rotation, roomMask);
        if(colliders.Length > 0)
        {
            foreach(Collider c in colliders)
            {
                if (c.transform.parent.gameObject.Equals(r.gameObject))
                {
                    continue;
                }
                else
                {
                    Debug.LogError("Overlap detected.");
                    //Debug.Log(c.name);
                    return true;
                }
            }
        }
        return false;
    }
}
