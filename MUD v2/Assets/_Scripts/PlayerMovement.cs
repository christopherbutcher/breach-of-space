﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController charcon;
    public Transform cam;
    public Transform groundCheck;
    public LayerMask groundMask;

    public float groundDist = 2f;
    public float speed = 12f;
    public float turnSmoothTime = 0.1f;
    float jumpHeight = 3f;
    float turnSmoothVel;
    float grav = -9.81f;
    bool grounded;
    Vector3 velo;


    void Update()
    {
        grounded = Physics.CheckSphere(groundCheck.position, groundDist, groundMask);

        if(grounded && velo.y < 0)
        {
            velo.y = -2f;
        }

        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;
        
        if(direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) *
                Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, 
                targetAngle, ref turnSmoothVel, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            charcon.Move(moveDir.normalized * speed * Time.deltaTime);
        }

        // Jump

        if(Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            velo.y = Mathf.Sqrt(jumpHeight * -2f * grav);
        }
        velo.y += grav * 2 * Time.deltaTime;
        charcon.Move(velo * Time.deltaTime);
    }
}
