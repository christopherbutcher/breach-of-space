﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreatLevelMidnight : MonoBehaviour
{
    // The three textures for each
    //     thing in the game to
    //     switch between.
    public Texture low;
    public Texture mid;
    public Texture hi;

    void Start()
    {
        
    }

    public void Changes(int tl)
    {
        /////
        ///"_MainTex" is the main diffuse texture. 
        ///           This can also be accessed via
        ///           mainTexture property.
        ///"_BumpMap" is the normal map.
        ///"_LightMap" is the lightmap texture.
        ///"_Cube" is the reflection cubemap.)
        /////

        switch (tl)
        {
            case 1:
                GetComponent<Material>().SetTexture("_MainTex", low);
                break;
            case 2:
                GetComponent<Material>().SetTexture("_MainTex", mid);
                break;
            case 3:
                GetComponent<Material>().SetTexture("_MainTex", hi);
                break;
            default:
                Debug.LogError("Threat level out of bounds.");
                break;
        }
    }
}
